﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class playerHealth : MonoBehaviour {
    public int startingHealth = 100;
    public int currentHealth;
    public Slider healthSlider;
	public Image DamageImage;
    public float flashSpeed = 5f;
    public Color flashColour = new Color(1f, 0f, 0f, 0.1f);



    Animator anim;
    AudioSource playerAudio;
    Movement playerMovement;
    //PlayerShooting playerShooting;
    bool isDead;
    bool damaged;
    void Awake()
    {
        currentHealth = startingHealth;
		playerMovement = GetComponent<Movement> ();
    }
	
	// Update is called once per frame
	void Update () {
        if (damaged)
        {
            DamageImage.color = flashColour;
        }
        else
        {
            DamageImage.color = Color.Lerp(DamageImage.color, Color.clear, flashSpeed * Time.deltaTime);
        }
        damaged = false;

    }
    public void TakeDamage(int amount)
    {
        damaged = true;

        currentHealth -= amount;

        healthSlider.value = currentHealth;


        if (currentHealth <= 0 && !isDead)
        {
            Death();
        }
    }
    void Death()
    {
        isDead = true;
		playerMovement.enabled = false;
		Destroy (gameObject, 2f);
       
    }
    public void getHealth()
    {
        currentHealth += 50;
    }
    /*void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.CompareTag("pickup"))
        {
            if (other.gameObject.name == "healthPickup(Clone)")
            {
                currentHealth += 20;
                healthSlider.value = currentHealth;
                if (currentHealth > 100)
                {
                    currentHealth = 100;
                    healthSlider.value = 100;
                }
            }
            Destroy(other.gameObject);
        }
    }
    */
    public void RestartLevel()
    {
        //SceneManager.LoadScene(0);
    }

}
