﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraFollow : MonoBehaviour {

    public GameObject player;
    public float smoothing = 5f;
    public float rotationSpeed = 10;
    Vector3 offset;

    void Start()
    {
        offset = transform.position - player.transform.position;
    }
   /* void Update()
    {
        Vector3 rotation = transform.eulerAngles;
        rotation.y += Input.GetAxis("Horizontal") * rotationSpeed * Time.deltaTime;
        transform.eulerAngles = rotation;
    }*/
    void LateUpdate()
    {
        this.transform.position = player.transform.position + offset;
    }
}
