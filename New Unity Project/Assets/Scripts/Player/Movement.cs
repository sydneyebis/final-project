﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {
    //player variables
    public GameObject button;
    public playerHealth playerHP;
    //playermovement variables
    private UnityEngine.AI.NavMeshAgent navMeshAgent;
    private Ray shootRay;
	private RaycastHit shootHit;
	private bool walking;
	private bool enemyClicked;
   //enemy variables
    public static bool imFound = false;
    enemyMovement enemyMove;
    enemyHealth enemyHP;
   //attacking variables
	public float shootdistance = 10f;
    public float shootRate = .5f;
	public Collider targetedEnemy;
	private float nextFire;
	public int damage = 10;
    public int biteDamage = 50;
	public float attackDelay = 0.5f; 
	float distance = 1000f;
	float attackTimer = 0;
	float loseTargetDelay = 0f;
	// Use this for initialization
	void Awake () {
       
		navMeshAgent = GetComponent<UnityEngine.AI.NavMeshAgent>();
	}
	
	// Update is called once per frame
	void Update ()
    {
    //movement
		loseTargetDelay += Time.deltaTime;
		attackTimer += Time.deltaTime;
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
		if (Input.GetButtonDown ("Fire1"))
		{
			if(Physics.Raycast(ray, out hit, 100))
			{
				if (hit.collider.CompareTag("enemy"))
				    {
						targetedEnemy = hit.collider;
                    enemyClicked = true;
						enemyHP = targetedEnemy.GetComponent<enemyHealth>();
						distance = Vector3.Distance(transform.position, targetedEnemy.transform.position);
						loseTargetDelay = 0;
				    }
				if(loseTargetDelay > 5 && enemyClicked == true)
				{
					enemyClicked = false;
				}
				else
				{

						walking = true;
						navMeshAgent.destination = hit.point;
						navMeshAgent.Resume();
				}
			}
		}
 //attacking
		if(Input.GetButtonDown("Fire2"))
		{
			
		}
		if(Input.GetButtonDown("Fire2") && enemyClicked == true && distance <2 && attackTimer> attackDelay){
            if (imFound == false)
            {

                biteAttack();
            }
            else { attack(); }
		}
	}
    void attack()
    {
        Debug.Log("stab");
		enemyHP.takeDamage(damage);
	}
    void biteAttack()
    {
        Debug.Log("bite");
        enemyHP.takeDamage(biteDamage);
        playerHP.getHealth();

    }
//stealth
    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("enemy"))
        {
            enemyMovement.playerInZone = true;


        }
    }
     void OnTriggerEnter(Collider other)
    {
            if (other.gameObject.CompareTag("item"))
            {
                Destroy(other.gameObject);
                button.SetActive(true);
              int click =  button.GetComponent<introMessages>().numClicks;
            if (click < 10)
            {
                button.GetComponent<introMessages>().numClicks = 10;
                button.GetComponent<introMessages>().goalText2.SetActive(true);
            }
            else {
                button.GetComponent<introMessages>().numClicks = 15;
            }
            }
    }
    void OnTriggerExit(Collider other)
    {
        enemyMovement.playerInZone = false;    
    }

}
