﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyHealth : MonoBehaviour {
	public int startingHealth = 20;
	public int currentHealth;
	bool isDead;
	enemyMovement moving;
	enemyAttack attack;
	public int defence = 0;
	// Use this for initialization
	void Start () {
		moving = GetComponent<enemyMovement> ();
		attack = GetComponent<enemyAttack> ();
		currentHealth = startingHealth;
	}
	
	// Update is called once per frame
	void Update () {
		if (currentHealth<0)
		{
			death ();
		}
	}
	public void takeDamage(int amount){
		currentHealth -= (amount - defence);
		Debug.Log ("I was hit");
	}
	void death(){
		moving.enabled = false;
		attack.enabled = false;

		Destroy (gameObject,1f);
	}
}
