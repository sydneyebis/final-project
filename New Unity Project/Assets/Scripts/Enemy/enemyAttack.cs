﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyAttack : MonoBehaviour {
    GameObject player;
    bool playerInRange = false;
    public int attackDamage = 5;
    public float attackRate = 0.5f;
    float timer = 0;
    float distance;
    playerHealth PlayerHealth;

    // Use this for initialization
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        PlayerHealth = player.GetComponent<playerHealth>();
       // Debug.Log("I found the starting health of: "+ PlayerHealth.currentHealth);
        distance = Vector3.Distance(player.transform.position, transform.position);
    }

	
	// Update is called once per frame
	void Update () {
        timer += Time.deltaTime;
        //Debug.Log(timer);
        if (enemyMovement.playerFound == true){
            playerDistance();
        }
        if (distance<2 && PlayerHealth.currentHealth > 0 && timer >= attackRate)
        {
            attack();
            //Debug.Log(distance);
            
            timer = 0;
        }
	}
    void attack()
    {
        //Debug.Log("Ouch");
        PlayerHealth.TakeDamage(attackDamage);
    }
    void playerDistance()
    {
            distance = Vector3.Distance(player.transform.position, transform.position);
        //Debug.Log(distance);
    }
}
