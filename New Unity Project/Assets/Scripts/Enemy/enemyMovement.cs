﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class enemyMovement : MonoBehaviour
{
	public Transform[] patrolPoints;
	//public Transform nextPatrol;
    public GameObject player;
    Movement playerMove;
    playerHealth playerHealth;
    UnityEngine.AI.NavMeshAgent nav;
    public static bool playerFound = false;
    RaycastHit hitInfo;
    public static bool playerInZone = false;
	public int destPoint = 0;
	// Use this for initialization
	void Awaken ()
    {
		//player = GameObject.FindGameObjectWithTag("Player");
        playerHealth = player.GetComponent<playerHealth>();
        playerMove = player.GetComponent<Movement>();
        
        
	}
    void Start()
    {
        nav = GetComponent<UnityEngine.AI.NavMeshAgent>();
        playerFound = false;
       // Debug.Log(playerFound);
        
    }
	void findPlayer() {
              //  Debug.Log("near");
			//enemyMovement enemyMove = other.gameObject.GetComponent<enemyMovement>();
			if (Physics.Linecast (gameObject.transform.position, player.transform.position, out hitInfo)) {
                // Debug.Log(hitInfo.collider);
				if (hitInfo.collider.CompareTag ("Player") && hitInfo.distance<8) { 
                playerFound = true;
                Movement.imFound = true;
                //Debug.Log("found you");
				}
                else
                {
               // Debug.Log("false");
					playerFound = false;
				}
			}
		}
	void patrol()
	{ 
		nav.destination = patrolPoints[destPoint].position;
		destPoint = (destPoint + 1) % patrolPoints.Length;
		

	}

    // Update is called once per frame
    void Update ()
    {
		
        if (playerInZone == true)
        {
            findPlayer();
         
        }

        if (playerFound == true) {
            nav.SetDestination(player.transform.position);
        }
        else if (!nav.pathPending && nav.remainingDistance < 0.5f && playerFound == false)
		patrol();

    }
}

   
