﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class introMessages : MonoBehaviour {
    public GameObject controlText;
    public GameObject winText;
    public GameObject narration1;
    public GameObject narration2;
    public GameObject narration3;
    public GameObject attackText;
    public GameObject goalText1;
    public GameObject goalText2;
    public GameObject player;
    public Movement playerMove;

    public int numClicks = 0;
	// Use this for initialization
	void Start () {
        player = GameObject.FindGameObjectWithTag("Player");
        playerMove = player.GetComponent<Movement>();
        playerMove.enabled = false;
    }
	
	// Update is called once per frame
	void Update () {
       
	}
    public void onClick()
    {
        numClicks ++;
        if (numClicks == 16) {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
        if (numClicks == 15) {
            winText.SetActive(true);
        }
        if (numClicks == 11) {
            goalText2.SetActive(false);
            gameObject.SetActive(false);
        }
        if (numClicks == 10)
        {
            goalText2.SetActive(true);
        }
        if (numClicks ==  6)
        {
            playerMove.enabled = true;
            gameObject.SetActive(false);
            goalText1.SetActive(false);
        }
        if (numClicks == 5) {
            attackText.SetActive(false);
            goalText1.SetActive(true);
        }
        if (numClicks == 3) {
            controlText.SetActive(true);
            narration3.SetActive(false);
        }
        if (numClicks == 4) {
            controlText.SetActive(false);
            attackText.SetActive(true);
        }
        if (numClicks == 1) {
            narration1.SetActive(false);
            narration2.SetActive(true);
        }
        if (numClicks == 2) {
            narration2.SetActive(false);
            narration3.SetActive(true);
        }

    }
}
